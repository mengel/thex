function [tHexPath, slabThickness] = CalculateTHex(tHexCoord)
% Calculate T-Hex grid from T-Hex coordinates
%
%   [tHexPath, slabThickness] = CalculateTHex(tHexCoord)
%
% IN
%   tHexCoord       x and y coordinate of point in skew coordinate system
%                   of hexagonal grid [2 x 1]
%
% OUT
%   tHexPath        [N x 2] matrix, that contains ky and kz coordinates of
%                   grid points of one shot until pattern repeats
%   slabThickness   k-space extent, that one shot will cover in kz
%                   direction, given in multiples of delta_k ('d' in Fig 2
%                   of T-Hex paper), for stacking shots, they have to be
%                   shifted by slabThickness in kz direction
%
% EXAMPLE
%   [tHexPath, slabThickness] = CalculateTHex([3 1])
%
%   See also GetDistancesInHexGrid GetTHexGeometry GetTHexPointPosition PlotTHex
%
% Maria Engel (c) 2020-01-09 IBT-ETH. Contact IBT-ETH before using this code.

%% Calculate T-Hex
[slabThickness, parallelDist, ~, coordDistOrth, parallelDistOrth] = ...
    GetTHexGeometry(tHexCoord);
fprintf('T-Hex coordinate = [%i,%i] \n',tHexCoord(1),tHexCoord(2));
fprintf('This corresponds to slab thickness (multiples of delta-k): %G \n',slabThickness);

nPoints = round(coordDistOrth/parallelDist);
[pointHeight,nUniquePoints] = GetTHexPointPosition(tHexCoord, nPoints); % nPoints = nLaneChanges+1

tHexPath(:,1) = (0:(nUniquePoints-1))*parallelDist;
tHexPath(:,2) = pointHeight(1:nUniquePoints)*parallelDistOrth;