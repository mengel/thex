% DemoTHex calculates T-Hex grid for demonstration
%
% USE
%   DemoTHex
%
% This code helps you generate T-Hex trajectories, it provides the desired
% T-Hex scheme of lane changes, which you may apply to your Spirals/EPIs
% etc. This script will let you specify a T-Hex coordinate or a slab
% thickness (multiples of delta_k ('d' in Paper Fig 2)). 
% In the latter case, it will find that tHexCoord, which matches
% the chosen thickness most closely. It will then calculate the according
% T-Hex path and plot it on a hexagonal grid for two adjacent shots.
%
% See also GetDistancesInHexGrid GetTHexGeometry GetTHexPointPosition CalculateTHex
%
% Author(s): Maria Engel
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

%% Collect input (thickness or tHex-coordinate)
prompt = 'Do you want to specifiy T-Hex coordinate (c) or slab thickness (t)? [c]: ';
inputType = input(prompt,'s');

if isempty(inputType)
    inputType = 'c';
end

if strcmp(inputType,'c')
    tHexCoord = input('Choose T-Hex coordinate (e.g. [3 1]):');
    if isempty(tHexCoord)
        tHexCoord = [3 1];
    end
elseif strcmp(inputType,'t')
    chosenDistance = input('Slab thickness in multiples of delta-k (e.g. 3.6):');
    if isempty(chosenDistance)
        chosenDistance = 4;
    end
    tHexCoord = GetTHexCoordFromDistance(chosenDistance);
else
    error('I�m sorry, I did not understand your input. Please try again! You have to type ''c'' or ''t''.');
end

% Prevent invalid T-Hex coordinate inputs (only green rings in yellow wedge
% in Figure 2 in T-Hex paper are appropriate)
errorString = 'The chosen T-Hex coordinate is not applicable, ';
if gcd(tHexCoord(1),tHexCoord(2))>1
    error([errorString 'entries have to be coprime.']);
elseif tHexCoord(2)>tHexCoord(1) && ~(tHexCoord(1)==1 && tHexCoord(2)==1)
    error([errorString 'second entry has to be smaller than first entry.']);
end

[tHexPath, slabThickness] = CalculateTHex(tHexCoord);
PlotTHex(tHexPath, slabThickness)

disp(['Hint: Use workspace variable tHexPath to create a hexagonal grid ' ...
    'in k-space by shifting and replicating']);