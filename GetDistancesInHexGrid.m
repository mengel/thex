function [availableDistances, availableTHexCoord] = ...
    GetDistancesInHexGrid(distMax,doExcludeCoprimes,doPlot)
% Calculates distances (--> slab thicknesses) available in hexagonal grid
%
%   [availableDistances, availableTHexCoord] = GetDistancesInHexGrid(distMax,doExcludeCoprimes,doPlot)
%
% IN
%   distMax             [20] maximum distance to be taken into account
%   doExcludeCoprimes   [false] Discard a distance if x and y coordinate
%                       are coprime (= irrelevant for blipped T-Hex)
%   doPlot              [false] Plot available distances
%
% OUT
%   availableDistances  [#distances] Distances available in hexagonal grid,
%                       up to distMax
%   availableTHexCoord  [2 x #distances] T-Hex coordinates corresponding to
%                       availableDistances
%
% EXAMPLE
%   GetDistancesInHexGrid(20)
%
% See also DemoTHex
%
% Author(s): Maria Engel
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if nargin < 1
    distMax = 20;
end
if nargin < 2
    doExcludeCoprimes = false;
end

if nargin < 3
    doPlot = false;
end

distMax = ceil(distMax);

i = 0;
distance = [];
% Loop over tHexCoord_X (c) and tHexCoord_Y (b)
for c = 1:distMax
    for b = 0:c
        distanceTmp = sqrt(b^2+c^2+b*c);
        
        % Do not count points of same distance twice
        % if 2 points have same distance and one is reachable without
        % tilting the grid, count this one
        % Do exlude point if coordinates are coprime (= point not valid for
        % blipped T-Hex
        if any(find(distance==distanceTmp)) || ...
                (mod(distanceTmp,1) == 0 && b>0 && ~doExcludeCoprimes) || ...
                (gcd(c,b)>1 && doExcludeCoprimes)
            continue;
        end
        i = i+1;
        distance(i) = distanceTmp;
        tHexCoord(:,i) = [c,b];
        if b == 0
            nonHexMarkerDistance(i) = distance(i);
        else
            nonHexMarkerDistance(i) = NaN;
        end
    end
end

[distanceSorted, order] = sort(distance);
nonHexMarkerDistanceSorted = nonHexMarkerDistance(order);
tHexCoordSorted = tHexCoord(:,order);

availableDistances = distanceSorted(distanceSorted<=distMax);
nonHexMarkerDistanceSortedCutoff = nonHexMarkerDistanceSorted(distanceSorted<=distMax);
availableTHexCoord = tHexCoordSorted(:,distanceSorted<=distMax);

if doPlot
    markerSize = 12;
    
    figure('Name','Distance'); hold on;
    plot(distance,'.','MarkerSize',markerSize);
    plot(nonHexMarkerDistance,'.','MarkerSize',markerSize);
    axis([0 numel(distance) 0 max(distance)]);
    set(gca,'box','off','XTick',[],'XColor','w');
    ylabel('distance [multiples of the lattice constant]');
    legend('T-Hex distances', 'Distances available without grid tilting');
    
    figure('Name','Distance sorted'); hold on;
    plot(distanceSorted,'.','MarkerSize',markerSize);
    plot(nonHexMarkerDistanceSorted,'.','MarkerSize',markerSize);
    axis([0 numel(distance) 0 max(distance)]);
    set(gca,'box','off','XTick',[],'XColor','w');
    ylabel('distance [multiples of the lattice constant]');
    legend('T-Hex distances', 'Distances available without grid tilting');
    
    figure('Name','Distance sorted cutoff'); hold on;
    plot(availableDistances,'.','MarkerSize',markerSize);
    plot(nonHexMarkerDistanceSortedCutoff,'.','MarkerSize',markerSize);
    axis([0 numel(availableDistances) 0 max(availableDistances)]);
    set(gca,'box','off','XTick',[],'XColor','w');
    ylabel('distance [multiples of the lattice constant]')
    legend('T-Hex distances', 'Distances available without grid tilting');
    
    figure('Name','Example 0p5mm R4'); hold on;
    TAQ = [140.1 70.13 48.42 36.36 29.13 24.31 21.9 19.49];
    nInterleaves = 8;
    nTAQTHex = sum(availableDistances<nInterleaves);
    TAQTHex = TAQ(1)./availableDistances(1:nTAQTHex);
    plot(availableDistances(1:nTAQTHex),TAQTHex,'.','MarkerSize',markerSize);
    plot(1:nInterleaves,TAQ(1:nInterleaves),'.','MarkerSize',markerSize);
    xlabel('# interleaves per k-space plane');
    ylabel('TAQ per shot [ms]');
    axis([0.8 nInterleaves TAQ(nInterleaves)-1 TAQ(1)+5]);
    legend('T-Hex TAQ/shot (approximate!)', 'TAQ/shot available without grid tilting');
end