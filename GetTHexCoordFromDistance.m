function tHexCoord = GetTHexCoordFromDistance(slabThickness)
% Find that tHexCoord, which matches the chosen slab thickness most closely
%
% USE
%   tHexCoord = GetTHexCoordFromDistance(slabThickness)
%
% IN
%   slabThickness   k-space extent, that one shot should cover in kz
%                   direction, given in  multiples of delta_k ('d' in Fig 2
%                   of T-Hex paper), for stacking shots, they have to be
%                   shifted by slabThickness in kz direction
%
% OUT
%   tHexCoord       x and y coordinate of point in skew coordinate system
%                   of hexagonal grid [2 x 1]
%
%
% EXAMPLE
%   GetTHexCoordFromDistance(4)
%
% See also GetDistancesInHexGrid GetTHexGeometry GetTHexPointPosition
%
% Author(s): Maria Engel
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland
  
[availableDistances, availableTHexCoord] = ...
    GetDistancesInHexGrid(slabThickness,true,false);
[~, ind] = min(abs(availableDistances-slabThickness));
tHexCoord = availableTHexCoord(:,ind);