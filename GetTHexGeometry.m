function [coordDist, parallelDist, tHexCoordOrth, coordDistOrth, ...
    parallelDistOrth, distanceRatio] = GetTHexGeometry(tHexCoord)
% GetTHexGeometry calculates distances as they appear in hexagonal grid for
% specified tilting scenario
%
% USE
%   GetTHexGeometry(tHexCoord)
%
% IN
%   tHexCoord           x and y coordinate of point in skew coordinate
%                       system of hexagonal grid [2 x 1]
%
% OUT
%   coordDist           distance between origin and tHexCoord
%   parallelDist        distance between neighboring lines parallel to
%                       (origin --> tHexCoord), which hit one, and only
%                       one point in slab orthogonal to (ori. --> tHexCoord)
%   tHexCoordOrth       Coordinates of point in hex. grid, which describes
%                       repetitiveness of the sampling scheme ('w' in
%                       T-Hex paper), tHexCoordOrth is orthogonal to tHexCoord
%   coordDistOrth       distance between origin and tHexCoordOrth
%   parallelDistOrth    parallelDist for tHexCoordOrth
%   distanceRatio       rotation increment for non-blipped T-Hex
%
% EXAMPLE
%   [coordDist, parallelDist, tHexCoordOrth, coordDistOrth, ...
%     parallelDistOrth, distanceRatio] = GetTHexGeometry([2 1])
%
% See also GetTHexPointPosition
%
% Author(s): Maria Engel
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

coordDist = CoordinateDistance(tHexCoord(1),tHexCoord(2));
tHexCoordOrth = [-(2*tHexCoord(2)+tHexCoord(1)), 2*tHexCoord(1)+tHexCoord(2)];
tHexCoordOrth = tHexCoordOrth/gcd(tHexCoordOrth(1),tHexCoordOrth(2));

% Find point [x y] with x>y (yellow wedge in Fig.2 of T-Hex paper) and same
% distance to origin as tHexCoordOrth
analogToOrthCoord = [-tHexCoordOrth(1), tHexCoordOrth(2)+tHexCoordOrth(1)];

coordDistOrth = CoordinateDistance(analogToOrthCoord(1),analogToOrthCoord(2));

neighbourCoordDist = CoordinateDistance(tHexCoord(1)-1,tHexCoord(2));

if neighbourCoordDist > 0
    parallelDistNeighbour = HeronsFormula(coordDist,1,neighbourCoordDist);    
    parallelDist = parallelDistNeighbour/tHexCoord(2);
else % Extra treatment for tHexCoord(2) = 0 (on x axis)
    parallelDistNeighbour = HeronsFormula(1,1,1);
    parallelDist = parallelDistNeighbour;
end

if nargout>4 % avoid infinite recursion
    [~, parallelDistOrth] = GetTHexGeometry(analogToOrthCoord);
end

if neighbourCoordDist > 0
    distanceRatio = sqrt(neighbourCoordDist^2-parallelDistNeighbour^2)/tHexCoord(2)/coordDist;
else % Extra treatment for tHexCoord(2) = 0 (on x axis)
    distanceRatio = 0.5;
end
end

function dist = CoordinateDistance(x,y)
% Calculates distance from origin for given point in 2D hexagonal
% coordinate system (x,y)
dist = sqrt(x^2+y^2+x*y);
end

function h = HeronsFormula(a,b,c)
% Calculates height h on side a of triangle with side lenghts a, b and c
s = (a+b+c)/2;
h = 2*sqrt(s*(s-a)*(s-b)*(s-c))/a;
end