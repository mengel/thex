function [pointHeight,nUniquePoints] = GetTHexPointPosition(tHexCoord, nLaneChanges)
% GetTHexPointPosition calculates distances as they appear in hexagonal
% grid for different tilting scenarios
%
% USE
%   GetTHexPointPosition(tHexCoord, nLaneChanges)
%
% IN
%   tHexCoord           x and y coordinate of point in skew coordinate
%                       system of hexagonal grid [2 x 1]
%   nLaneChanges        number of lane changes ('z-blips') in T-Hex trajectory
%
% OUT
%   pointHeight         array of length of nLaneChanges, which contains the
%                       k-z position that is reached after each lane change,
%                       starting with 0; given in multiples of
%                       orthParallelDist --> as defined in GetTHexGeometry
%   nUniquePoints       #lane changes before T-Hex pattern repeats
%                       = distance between origin and tHexCoord, it is
%                       always 0 < pointHeight < nUniquePoints;
%                       same unit as pointHeight
%
% EXAMPLE
%   pointHeight = GetTHexPointPosition([2, 1], 50)
%
% See also GetTHexGeometry
%
% Author(s): Maria Engel
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

if nargin < 2
    nLaneChanges = [];
end

[~, ~, tHexCoordOrth] = GetTHexGeometry(tHexCoord);

gridSize = max(max(abs(tHexCoord)),max(abs(tHexCoordOrth)))*10;
grid = zeros(gridSize,gridSize);
val = (1:gridSize)-ceil(gridSize/2);


for i = 1:gridSize
    for j = 1:gridSize
        if val(j) > straight(tHexCoord,val(i)) && ...
                val(j) > straight(tHexCoordOrth,val(i)) && ...
                val(j) < straightShifted(tHexCoord,val(i),tHexCoordOrth) && ...
                val(j) < straightShifted(tHexCoordOrth,val(i),tHexCoord)
            
            grid(i,j) = 1;
        end
    end
end
nUniquePoints = sum(sum(grid))+1;
if tHexCoord(2) > 0
    pointHeight = (nUniquePoints-tHexCoordOrth(2))/tHexCoord(2);
else
    pointHeight = 1;
end

if isempty(nLaneChanges)
    nLaneChanges = nUniquePoints;
end

for iLaneChange = 2:nLaneChanges
    pointHeight(iLaneChange) = mod(pointHeight(1)*iLaneChange,nUniquePoints);
end

pointHeight = [0 pointHeight];
end

function y = straight(P,x)
y = P(2)/P(1)*x;
end

function y = straightShifted(P,x,Q)
y = P(2)/P(1)*(x-Q(1))+Q(2);
end