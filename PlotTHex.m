function PlotTHex(tHexPath, slabThickness)
% Plot T-Hex path on a hexagonal grid for two adjacent shots.
%
% USE
%   PlotTHex(tHexPath, slabThickness)
%
% IN
%   tHexPath        [N x 2] matrix, that contains ky and kz coordinates of
%                   grid points of one shot until pattern repeats
%   slabThickness   k-space extent, that one shot will cover in kz
%                   direction, given in  multiples of delta_k ('d' in Fig 2
%                   of T-Hex paper), for stacking shots, they have to be
%                   shifted by slabThickness in kz direction
%
% EXAMPLE
%   [tHexPath, slabThickness] = CalculateTHex([2 1]);
%   PlotTHex(tHexPath, slabThickness)
%
% See also GetDistancesInHexGrid GetTHexGeometry GetTHexPointPosition
%
% Author(s): Maria Engel
% (c) Institute for Biomedical Engineering, ETH and University of Zurich, Switzerland

figure('Name','T-Hex lane change scheme'); hold all;

%% Coloring like in paper...
borderColor     = [191 191 191]/255;    % gray lines to mark borders between shots
gridColour      = [166 166 166]/255;    % gray grid points
lineColour      = [255 192 0]/255;      % yellow T-Hex lines
repeatColor     = [178 139 217]/255;    % purple vertical line @repetition point

lineWidth       = 3;
auxLineWidth    = 2;
markerSize      = 30;

%% Extent of plot
nShots          = 2;    % #shots to be plotted
bufferZone      = 2;    % Plot grid also around T-Hex path (given in delta_k)
plottingWidth   = 10;   % Extent of hex. grid in plot (given in delta_k)

%% Derived parameters
% Corresponding to coil distance in Archimedean Spiral
parallelDist    = tHexPath(2,1)-tHexPath(1,1);
% #lane changes after which T-Hex pattern is repeated
nUniquePoints   = size(tHexPath,1);
tHexCoord       = GetTHexCoordFromDistance(slabThickness);
coordDistOrth   = parallelDist*nUniquePoints;
% Extra shots required for buffer zone
nExtraShots     = ceil(bufferZone/slabThickness);

% Extent of T-Hex path
lineRange       = 1:ceil((plottingWidth-bufferZone)/parallelDist);
% Extend given points as far as background grid should reach
tHexGrid        = repmat(tHexPath,ceil(plottingWidth/tHexPath(end,1)),1);
tHexGrid(:,1)   = (0:size(tHexGrid,1)-1)*parallelDist;

%% Actual plot
for iShot = 1:nShots+2*nExtraShots
    if iShot <= nShots+1
        plot((lineRange-1)*parallelDist,ones(lineRange(end),1)*slabThickness*(iShot-1),...
            'LineStyle','-','Color',borderColor,'LineWidth',auxLineWidth);
    end
    
    plot(tHexGrid(:,1),tHexGrid(:,2)+slabThickness*(iShot-nExtraShots-1),'Color',gridColour,...
        'Marker','.','MarkerSize',markerSize,'LineStyle','none');
    
    if iShot > nExtraShots && iShot < nShots+nExtraShots+1
        plot(tHexGrid(lineRange,1),tHexGrid(lineRange,2)+slabThickness*(iShot-nExtraShots-1),...
            'Color',lineColour,'LineWidth',lineWidth);
    end
end

plot([coordDistOrth coordDistOrth],[0 nShots*slabThickness],...
    'Color',repeatColor,'LineWidth',auxLineWidth);

axis equal;
xlim([0 plottingWidth]);
ylim([-bufferZone slabThickness*nShots+bufferZone]);
xlabel('k_y [\Deltak]');
ylabel('k_z [\Deltak]');
title(sprintf('T-Hex coordinate: [%i,%i]',tHexCoord(1),tHexCoord(2)));