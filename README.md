# Purpose

This code is related to the paper "T-Hex: Tilted hexagonal grids for rapid 3D imaging" in Magnetic Resonance Imaging.

This code helps you generate T-Hex trajectories, it provides the desired T-Hex scheme of lane changes, which you may apply to your Spirals/EPIs etc.
Details can be found in <paper link>


# How to cite 

If you use this code, please cite:

T-Hex: Tilted hexagonal grids for rapid 3D imaging
in Magnetic Resonance Imaging.
M. Engel, L. Kasper, B. Wilm, B. Dietrich, L. Vionnet, F. Hennel, J. Reber, K.P. Pruessmann.
submitted (2020)



# Getting Started

1. Please download Matlab code from https://gitlab.ethz.ch/mengel/thex

2. Run `DemoTHex.m`
    - You will be asked to specify the T-Hex coordinate itself or the desired slab thickness.
    - The according T-Hex path will be calculated and visualized on a hexagonal grid for two adjacent shots.
    - The plot relates to Fig. 2 in the paper mentioned above.
    
3. Integrate T-Hex in your own code using
    
    ```
    tHexCoord = GetTHexCoordFromDistance(myPreferedSlabThickness);
    [tHexPath, actualSlabThickness] = CalculateTHex(tHexCoord);
    PlotTHex(tHexPath, actualSlabThickness)
    ```

4. Shift & replicate tHexGrid as you need and apply to your trajectories.


# Contact

Please report bugs or remarks to engel@biomed.ee.ethz.ch.

```
       ___                     ___           ___           ___     
      /\  \                   /\__\         /\  \         |\__\    
      \:\  \                 /:/  /        /::\  \        |:|  |   
       \:\  \               /:/__/        /:/\:\  \       |:|  |   
       /::\  \   _______   /::\  \ ___   /::\~\:\  \      |:|__|__ 
      /:/\:\__\ /\______\ /:/\:\  /\__\ /:/\:\ \:\__\ ____/::::\__\
     /:/  \/__/ \/______/ \/__\:\/:/  / \:\~\:\ \/__/ \::::/~~/~
    /:/  /                     \::/  /   \:\ \:\__\    ~~|:|~~|    
    \/__/                      /:/  /     \:\ \/__/      |:|  |    
                              /:/  /       \:\__\        |:|  |    
                              \/__/         \/__/         \|__|
```